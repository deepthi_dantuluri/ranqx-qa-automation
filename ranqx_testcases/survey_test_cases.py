import json
import unittest
from ranqx_util_files.ranqx_helper import RanqxHelper
from ranqx_tc_methods.survey_test_methods import SurveyTestMethods
from ranqx_tc_methods.signup_user_methods import SignupUserMethods
import pkg_resources


class SurveyTestCases(unittest.TestCase, RanqxHelper, SurveyTestMethods, SignupUserMethods):

    session = None
    test_input = None
    data = None
    a = None
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        if SurveyTestCases.session is None:
            print "inside survey init"
            RanqxHelper.__init__(self)
            resource_package = 'test_input_details_123'  # Could be any module/package name
            resource_path = '/survey_test_data.json'
            data = pkg_resources.resource_string(resource_package, resource_path)
            SurveyTestCases.test_input = json.loads(data)
            SurveyTestCases.session = True
    #
    # @classmethod
    # def setUpClass(self):
    #     print "in setup"
    #     self.a = RanqxHelper()
    #     self.a.access_ranqx_class()
    #
    # @classmethod
    # def tearDownClass(self):
    #     print "in teardown"
    #     self.a.browser_close()

    def setUp(self):
        print "in setup"
        self.access_ranqx()

    def tearDown(self):
        self.browser_close()

    def test0001_validate_home_page_navigation_options(self):
        """
          To verify home page navigation options
        """
        data = SurveyTestCases.test_input
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.validate_login_bank_abb(data["tabs"])


    def test0002_validate_menu_options_of_load_numbers(self):
        """
           To verify menu options of load numbers
        """
        data = SurveyTestCases.test_input
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.click_on_link("Load the numbers")
        self.verify_dropdown(data["load_numbers_dropdown"])

    # def test0003_validate_menu_options_for_view_report(self):
    #     data = self.test_input
    #     self.click_on_link("Sign in")
    #     self.enter_login_details(data["username"], data["password"])
    #     self.click_on_link("View my report")
    #     self.verify_dropdown(data["view_report_dropdown"])
    #
    # def test0004_verify_get_feedback_go_link(self):
    #     data = self.test_input['free_user']
    #     self.click_on_get_started()
    #     self.click_on_starter_plan("starter")
    #     self.to_fill_get_started_page(data["get_started_details"])
    #     self.to_validate_registration_page()
    #     self.to_click_on_confirmation_link()
    #     self.to_fill_business_name(data["business_name"])
    #     self.to_fill_trade_n_staff(data["trade_staff"])
    #     self.to_fill_auto_complete_data(data["autocomplete_values"])
    #     self.to_select_dropdown_values(data["dropdown_values"])
    #     self.close_got_it()
    #     self.to_submit_registration_form()
    #     self.click_on_link("Get feedback")
    #     self.click_on_link("Go")
    #     self.link_displayed_or_not("Reset Incentives")
    #
    # def test0005_verify_incentive_On(self):
    #     data = self.test_input
    #     self.click_on_link("Sign in")
    #     self.enter_login_details(data["username"], data["password"])
    #     self.to_click_toggle_option("Settings")
    #     self.set_incentive_option("On")
    #     self.incentive_pane_display()
    #
    # def test0006_set_charity_incentive(self):
    #     data = self.test_input
    #     self.click_on_link("Sign in")
    #     self.enter_login_details(data["username"], data["password"])
    #     self.to_click_toggle_option("Settings")
    #     self.set_incentive_option("On")
    #     self.set_charity_name(data["Charity_name"])
    #     self.set_amount_per_response(data["amt_per_response"])
    #     self.set_max_donation_amount(data["max_amt"])
    #     self.close_got_it()
    #     self.click_on_button("submit")
    #
    # def test0007_verify_incentives_in_survey_link(self):
    #     data = self.test_input
    #     self.click_on_link("Sign in")
    #     self.enter_login_details(data["username"], data["password"])
    #     self.click_on_link("Get feedback")
    #     self.click_on_link("QR or URL Link Survey")
    #     self.click_on_link(data["URL survey link"])
    #     self.switch_to_latest_window()
    #     self.verify_charity_display_on_link()
    #     self.verify_dollar_symbol()
    #     self.verify_piggy_pic(data["Charity_name"])
    #     self.verify_charity_names_in_survey_page(data["Charity_name"])
    #
    # def test0008_set_feedback_in_QR_survey_link(self):
    #     data = self.test_input
    #     self.click_on_link("Sign in")
    #     self.enter_login_details(data["username"], data["password"])
    #     self.click_on_link("Get feedback")
    #     self.click_on_link("QR or URL Link Survey")
    #     self.click_on_link(data["URL survey link"])
    #     self.switch_to_latest_window()
    #     self.drag_rating_slider(data["target_rating"])
    #     self.feedback_comments(data["comment"])
    #     self.drag_dollar_to_piggy("Charity1")
    #     self.click_on_button("submit")
    #
    # def test0009_verify_QR_survey_response_on_dashboard(self):
    #     data = self.test_input
    #     self.click_on_link("Sign in")
    #     self.enter_login_details(data["username"], data["password"])
    #     self.verify_feedback_response(data["target_rating"], data["comment"])

    if __name__ == '__main__':
        unittest.main()
        # #suite = unittest.TestLoader().loadTestsFromTestCase(SurveyTestCases)
        # unittest.TextTestRunner(verbosity=2).run(suite)

