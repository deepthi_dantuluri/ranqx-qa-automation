import json
import unittest

import pkg_resources
from ranqx_tc_methods.login_existing_users_methods import LoginExistingUsersMethods

from ranqx_util_files.ranqx_helper import RanqxHelper


class RanqxLoginExistingUsersTestCases(unittest.TestCase, RanqxHelper, LoginExistingUsersMethods):

    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        RanqxHelper.__init__(self)
        resource_package = "test_input_details_123"  # Could be any module/package name
        resource_path = '/login_existing_users_test_data.json'
        data = pkg_resources.resource_string(resource_package, resource_path)
        self.test_input = json.loads(data)

    def setUp(self):
        self.access_ranqx()
        print "Test case started executing"

    # To test existing free User login
    def test0010_Login_existing_business_user(self):
        print "Testing business user login..........."
        data = self.test_input["business_user"]
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.validate_login()
        self.to_click_toggle_option("Sign out")
        self.validate_logout()

    # To test existing free User login
    def test0011_Login_existing_free_user(self):
        print "Testing free user login..........."
        data = self.test_input["free_user"]
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.validate_login()
        self.to_click_toggle_option("Sign out")
        self.validate_logout()

    def test0012_Login_existing_accounting_partner(self):
        print "Testing accounting partner login..........."
        data = self.test_input["accounting_partner"]
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.validate_abb_bank_partner_login(data["tabs"])
        self.to_click_toggle_option("Sign out")
        self.validate_logout()

    def test0013_Login_existing_Industry_partner(self):
        print "Testing Industry partner login........"
        data = self.test_input["industry_partner"]
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.verify_page_header(data["title"])
        self.validate_industry_partner_login()
        self.to_click_toggle_option("Sign out")
        self.validate_logout()

    def test0014_Login_ASB_bank_admin(self):
        print "Testing Bank Admin login......"
        data = self.test_input["bank_admin"]
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.verify_page_header(data["title"])
        self.validate_login_bank_abb(data["tabs"])
        self.to_click_toggle_option("Sign out")
        self.validate_logout()

    def test0015_Login_ASB_bank_RM(self):
        print "Testing Bank RM login......"
        data = self.test_input["bank_rm"]
        self.click_on_link("Sign in")
        self.enter_login_details(data["username"], data["password"])
        self.verify_page_header(data["title"])
        self.validate_login_bank_abb(data["tabs"])
        self.to_click_toggle_option("Sign out")
        self.validate_logout()

    def tearDown(self):
        self.browser_close()

if __name__ == '__main__':
    unittest.main(verbose=2)
