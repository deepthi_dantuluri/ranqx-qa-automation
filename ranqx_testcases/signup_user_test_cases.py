import json
import unittest

import pkg_resources
from ranqx_util_files.ranqx_helper import RanqxHelper

from ranqx_tc_methods.signup_user_methods import SignupUserMethods


class SignupUsersTestCases(unittest.TestCase, RanqxHelper, SignupUserMethods):

    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        RanqxHelper.__init__(self)
        resource_package = "test_input_details_123"  # Could be any module/package name
        resource_path = '/signup_user_test_data.json'
        data = pkg_resources.resource_string(resource_package, resource_path)
        self.test_input = json.loads(data)

    def setUp(self):
        self.access_ranqx()
        print "TestCase: " + self._testMethodName
        print "Description: " + str(self._testMethodDoc)

    # Test case to sign up as starter
    def test_0001_signup_free_user(self):
        """
           To test ranqx user sign up as starter
        """
        data = self.test_input['free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])

    def test_0002_signup_business_NZ_monthly_plan(self):
        """
          To sign up for business NZ monthly plan
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0003_signup_business_NZ_monthly_plan_free(self):
        """
          To sign up for business NZ monthly plan with promo code free
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code"])
        self.click_on_link("Continue")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_free"])
        self.to_validate_next_billing_date(data["validity_free"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0004_signup_business_NZ_monthly_plan_roadoct16(self):
        """
          To sign up for business NZ monthly plan with promo code ROADOCT16
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code_50"])
        self.to_validate_order_page_after_promo_code(data["discount_amt"], data["gst_text_after_discount"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text_pc"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_roadoct16"])
        self.to_validate_next_billing_date(data["validity_roadoct16"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0005_signup_business_NZ_annually_plan(self):
        """
          To sign up for business NZ annual plan
        """
        data = self.test_input['business_annually_user']
        self.click_on_get_started()
        #self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0006_signup_business_NZ_annually_plan_pc_free(self):
        """
          To sign up for business NZ annual plan with promo code(Free)
        """
        data = self.test_input['business_annually_promo_code']
        self.click_on_get_started()
        # self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_click_continue()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0007_signup_business_NZ_annually_plan_pc_roadoct16(self):
        """
          To sign up for business NZ annual plan with promo code(ROADOCT16)
        """
        data = self.test_input['business_annually_promo_code']
        self.click_on_get_started()
        # self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code_50"])
        self.to_validate_order_page_after_promo_code(data["discount_amt"], data["gst_text_after_discount"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text_pc"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_pc"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)


    def test_0008_signup_business_aud_annual_plan(self):
        """
          To sign up for business au annual plan
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0009_signup_business_aud_annually_plan_pc_free(self):
        """
          To sign up for business au annual plan with free promo code
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code"])
        self.click_on_link("Continue")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_free"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0010_signup_business_aud_annually_plan_pc_simpleaus(self):
        """
          To sign up for business au annual plan with promo code simpleaus
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code_50"])
        self.to_validate_au_order_page_after_pc(data["payment_plan"], data["discount_amt"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text_pc"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_simpleaus"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0011_signup_business_aud_monthly_plan(self):
        """
          To sign up for business au monthly plan
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0012_signup_business_aus_monthly_plan_pc_free(self):
        """
          To sign up for business au monthly plan
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code"])
        self.click_on_link("Continue")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_free"])
        self.to_validate_next_billing_date(data["validity_free"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0013_signup_business_aus_monthly_plan_pc_simpleaus(self):
        """
          To sign up for business au monthly plan with promo SIMPLEAUS
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code_50"])
        self.to_validate_au_order_page_after_pc(data["payment_plan"], data["discount_amt"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text_pc"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_simpleaus"])
        self.to_validate_next_billing_date(data["validity_simpleaus"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0014_signup_aud_ranqx_user(self):
        """
          To test sign up for au ranqx user(starter plan)
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])

    def test_0015_signup_abb_silver_plan_pc_free(self):
        """
          To sign up for ABB silver plan with free promocode
        """
        data = self.test_input['abb_silver_pc']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_click_continue()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0016_signup_abb_silver_plan_pc_ABB12100(self):
        """
          To sign up for ABB silver plan with promocode: ABB12100
        """
        data = self.test_input['abb_silver_pc_abb12100']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_validate_order_page_after_promo_code(data["discount_amt"], data["gst_text_after_discount"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0017_signup_abb_platinum_plan_pc_free(self):
        """
          To sign up for ABB platinum plan with free promocode
        """
        data = self.test_input['abb_platinum_pc_free']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_click_continue()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0018_signup_abb_platinum_plan_pc_ABB1250P(self):
        """
          To sign up for ABB platinum plan with promo code : ABB1250P
        """
        data = self.test_input['abb_platinum_pc']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_validate_order_page_after_promo_code(data["discount_amt"], data["gst_text_after_discount"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])


    def test_0019_signup_abb_platinum_plan(self):
        """
          To sign up for ABB platinum plan
        """
        data = self.test_input['abb_platinum']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0020_signup_abb_silver_plan(self):
        """
          To sign up for ABB silver plan
        """
        data = self.test_input['abb_silver']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0021_signup_aud_silver_plan(self):
        """
          To sign up for AUD silver plan
        """
        data = self.test_input['aud_silver_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0022_signup_aud_silver_plan_pc_free(self):
        """
          To sign up for AUD silver plan with free promo code
        """
        data = self.test_input['aud_silver_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_click_continue()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_free"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0023_signup_aud_silver_plan_pc_abbau100s(self):
        """
          To sign up for AUD silver plan with promo code ABBAU100S
        """
        data = self.test_input['aud_silver_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code_50"])
        self.to_validate_au_order_page_after_pc(data["payment_plan"], data["discount_amt"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text_pc"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_aabau100s"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0024_signup_aud_platinum_plan(self):
        """
          To sign up for AUD Platinum plan
        """
        data = self.test_input['aud_platinum_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0025_signup_aud_platinum_plan_pc_free(self):
        """
          To sign up for AUD Platinum plan with free promo code
        """
        data = self.test_input['aud_platinum_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code"])
        self.to_click_continue()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_free"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0026_signup_aud_platinum_plan_pc_abbau50p(self):
        """
          To sign up for AUD Platinum plan with promo code (ABBAU50P)
        """
        data = self.test_input['aud_platinum_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_enter_promo_code(data["promo_code_50"])
        self.to_validate_au_order_page_after_pc(data["payment_plan"], data["discount_amt"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text_pc"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_aabau50p"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

        ############################## UPGRADE TEST CASES ##############################

    def test_0027_signup_upgrade_downgrade_data_business_monthly(self):
        """
            To upgrade ranqx user to business NZD monthly
         """
        data = self.test_input['upgrade_downgrade_data']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("monthly")
        self.click_on_button_by_text("Upgrade")
        self.to_validate_upgrade_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data["card_pay_text"])
        self.to_enter_card_details_upgrade(data["credit_card_details"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data["validation_data_upgrade_mon"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0028_signup_upgrade_downgrade_data_business_annual(self):
        """
            To upgrade ranqx user to business NZD Annual
         """
        data = self.test_input['upgrade_downgrade_data']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("annual")
        self.click_on_button_by_text("Upgrade")
        self.to_validate_upgrade_order_page(data["payment_plan_annual"], data["payment_gst_text_annual"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data["card_pay_text_annual"])
        self.to_enter_card_details_upgrade(data["credit_card_details"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data["validation_data_upgrade_annual"])
        self.to_validate_next_billing_date(data["validity_annual"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0029_upgrade_business_monthly_plan_annual(self):
        """
          To sign up for business NZ monthly plan to annual plan
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # Plan upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("annual")
        # clicks on update button
        self.click_on_button_by_text("Update")
        self.to_get_attribute_data_in_payment_page(data["validation_data_upgrade_annual"])
        self.to_validate_next_billing_date(data["validity_annual"])

    def test_0030_upgrade_free_business_monthly_promo_code(self):
        """
           Upgrade starter plan to monthly plan using promo code roadoct16 --RDP1145
           Failing due to difference in invoice mail and archived invoice.
        """
        data = self.test_input['upgrade_downgrade_data']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("monthly")
        self.click_on_button_by_text("Upgrade")
        self.to_validate_upgrade_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_enter_promo_code_upgrade_page(data["promo_code_4_upgrade"])
        self.to_validate_upgrade_order_page_after_pc(data["payment_plan"], data["gst_discount_after_pc"],
                                                     data["discount_amt_after_pc"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data["card_pay_text_pc"])
        self.to_enter_card_details_upgrade(data["credit_card_details"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data["validation_data_upgrade_mon_pc"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0031_upgrade_free_business_annual_promo_code(self):
        """
           Upgrade starter plan to annual plan using promo code roadoct16
        """
        data = self.test_input['upgrade_downgrade_data']
        data1 = self.test_input['annual_upgrade']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("annual")
        self.click_on_button_by_text("Upgrade")
        self.to_validate_upgrade_order_page(data1["payment_plan_annual"], data1["payment_gst_text_annual"])
        self.to_enter_promo_code_upgrade_page(data["promo_code_4_upgrade"])
        self.to_validate_upgrade_order_page_after_pc(data1["payment_plan_annual"], data1["gst_discount_after_pc"],
                                                     data1["discount_amt_after_pc"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text_annual"])
        self.to_enter_card_details_upgrade(data["credit_card_details"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data_upgrade_pc"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0032_upgrade_NZD_free_plan_abb_silver(self):
        """
          To sign up for NZD starter plan and upgrade to abb silver plan
        """
        data = self.test_input['upgrade_downgrade_data']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Upgrade your plan")
        data1 = self.test_input['abb_silver']
        self.to_select_abb_plan("silver")
        self.to_validate_order_page(data1["payment_plan"], data1["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0033_upgrade_AUD_free_plan_abb_silver(self):
        """
          To sign up for AUD starter plan and upgrade to abb silver plan
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Upgrade your plan")
        data1 = self.test_input['aud_silver_plan']
        self.to_select_abb_plan("silver")
        self.to_validate_au_order_page(data1["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0034_upgrade_NZD_monthly_plan_abb_silver(self):
        """
          Upgrade NZD monthly plan to abb silver
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['abb_silver']
        self.to_select_abb_plan("silver")
        self.to_validate_order_page(data1["payment_plan"], data1["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0035_upgrade_AUD_monthly_plan_abb_silver(self):
        """
          Upgrade AUD monthly plan to abb silver
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['aud_silver_plan']
        self.to_select_abb_plan("silver")
        self.to_validate_au_order_page(data1["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0036_upgrade_NZD_Annual_plan_abb_silver(self):
        """
          Upgrade NZD monthly plan to abb silver
        """
        data = self.test_input['business_annually_user']
        self.click_on_get_started()
        # self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['abb_silver']
        self.to_select_abb_plan("silver")
        self.to_validate_order_page(data1["payment_plan"], data1["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0037_upgrade_AUD_Annual_plan_abb_silver(self):
        """
          Upgrade AUD annual plan to aud abb silver
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        #plan upgrade
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['aud_silver_plan']
        self.to_select_abb_plan("silver")
        self.to_validate_au_order_page(data1["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0038_upgrade_NZD_free_ranqx_plan_abb_platinum(self):
        """
          Upgrade NZD free plan to abb platinum
        """
        data = self.test_input['upgrade_downgrade_data']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        data1 = self.test_input['abb_platinum']
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Upgrade your plan")
        self.to_select_abb_plan("platinum")
        self.to_validate_order_page(data1["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0039_upgrade_AUD_free_ranqx_plan_abb_platinum(self):
        """
          Upgrade AUD free plan to abb platinum
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Upgrade your plan")
        data1 = self.test_input['aud_platinum_plan']
        self.to_select_abb_plan("platinum")
        self.to_validate_au_order_page(data1["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0040_upgrade_NZD_monthly_plan_abb_platinum(self):
        """
          Upgrade NZD monthly plan to abb platinum
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['abb_platinum']
        self.to_select_abb_plan("platinum")
        self.to_validate_order_page(data1["payment_plan"], data1["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0041_upgrade_AUD_monthly_plan_abb_platinum(self):
        """
          Upgrade AUD monthly plan to abb platinum
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['aud_platinum_plan']
        self.to_select_abb_plan("platinum")
        self.to_validate_au_order_page(data1["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0042_upgrade_NZD_annual_plan_abb_platinum(self):
        """
          Upgrade NZD Annual plan to abb platinum
        """
        data = self.test_input['business_annually_user']
        self.click_on_get_started()
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['abb_platinum']
        self.to_select_abb_plan("platinum")
        self.to_validate_order_page(data1["payment_plan"], data1["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0043_upgrade_AUD_annual_plan_abb_platinum(self):
        """
          Upgrade AUD Annual plan to abb platinum
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        # Plan upgrade steps
        self.click_settings_menu_for("For Partners")
        self.click_on_link("Accountants apply here")
        data1 = self.test_input['aud_platinum_plan']
        self.to_select_abb_plan("platinum")
        self.to_validate_au_order_page(data1["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data1["card_pay_text"])
        self.to_enter_card_details(data1["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data1["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0044_upgrade_NZD_abb_silver_to_abb_platinum(self):
        """
          Upgrade NZD Silver plan to abb_platnium
        """
        data = self.test_input['abb_silver']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # Plan upgrade steps
        self.click_on_link("Manage my clients")
        self.click_on_link("Upgrade to the Platinum plan")
        data1 = self.test_input['abb_platinum']
        self.to_validate_upgrade_order_page(data1["payment_plan"], data1["payment_gst_text_upgrade"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0045_upgrade_AUD_abb_silver_to_abb_platinum(self):
        """
          Upgrade AUD Silver plan to abb_platnium
        """
        data = self.test_input['aud_silver_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("silver")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        # Plan upgrade steps
        self.click_on_link("Manage my clients")
        self.click_on_link("Upgrade to the Platinum plan")
        data1 = self.test_input['aud_platinum_plan']
        self.to_validate_aud_upgrade_order_page(data1["payment_plan_upgrade"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0046_upgrade_AUD_free_to_monthly(self):
        """
          Upgrade AUD free to monthly plan
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        #upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("monthly")
        data1 = self.test_input['au_business_month']
        self.click_on_button_by_text("Upgrade")
        self.to_validate_aud_upgrade_order_page(data1["payment_plan"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0047_upgrade_AUD_free_to_annual(self):
        """
          Upgrade AUD free to annual plan
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("annual")
        data1 = self.test_input['au_business_annual']
        self.click_on_button_by_text("Upgrade")
        self.to_validate_aud_upgrade_order_page(data1["payment_plan"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0048_upgrade_AUD_free_to_monthly_using_pc(self):
        """
          Upgrade AUD free to monthly plan using pc
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        #upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("monthly")
        data1 = self.test_input['au_business_month']
        self.click_on_button_by_text("Upgrade")
        self.to_validate_aud_upgrade_order_page(data1["payment_plan"])
        self.to_enter_promo_code_upgrade_page(data1["promo_code_50"])
        self.to_validate_au_upgrade_order_page_after_pc(data1["payment_plan"], data1["card_pay_text_upgrade_pc"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text_pc"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data_simpleaus"])
        self.to_validate_next_billing_date(data1["validity_simpleaus"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0049_upgrade_AUD_free_to_annual_using_pc(self):
        """
          Upgrade AUD free to annual plan using pc
        """
        data = self.test_input['au_free_user']
        self.click_on_get_started()
        self.click_on_starter_plan("starter")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_free_user_signup()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        # upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("annual")
        data1 = self.test_input['au_business_annual']
        self.click_on_button_by_text("Upgrade")
        self.to_validate_aud_upgrade_order_page(data1["payment_plan"])
        self.to_enter_promo_code_upgrade_page(data1["promo_code_50"])
        self.to_validate_au_upgrade_order_page_after_pc(data1["payment_plan"], data1["card_pay_text_upgrade_pc"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text_pc"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data_simpleaus"])
        self.to_validate_next_billing_date(data1["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)

    def test_0050_AUD_upgrade_business_monthly_plan_annual(self):
        """
          To sign up for business AUD monthly plan to annual plan
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        # Plan upgrade steps
        self.click_on_link("Change plan")
        self.click_on_upgrade_plan("annual")
        # clicks on update button
        self.click_on_button_by_text("Update")
        self.to_get_attribute_data_in_payment_page(data["validation_data_upgrade_annual"])
        self.to_validate_next_billing_date(data["validity_annual"])


        ################################## DOWNGRADE TEST CASES #########################################

    def test_0051_NZD_downgrade_annual_plan_to_monthly(self):
        """
          Downgrade NZD annual plan to monthly
        """
        data = self.test_input['business_annually_user']
        self.click_on_get_started()
        # self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_upgrade_plan("monthly")
        self.click_on_button_by_text("Update")
        data1 = self.test_input['upgrade_downgrade_data']
        self.to_validate_upgrade_order_page(data1["payment_plan"], data1["payment_gst_text"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text"])
        self.to_validate_text_in_payment_page(data1["payment_page_text"])
        self.to_enter_card_details_upgrade(data1["credit_card_details"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data_downgrade_monthly"])
        self.to_validate_next_billing_date(data1["validity_annual"])

    def test_0052_NZD_downgrade_annual_plan_to_starter_plan(self):
        """
          Downgrade NZD annual plan to starter plan
        """
        data = self.test_input['business_annually_user']
        self.click_on_get_started()
        # self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_button_by_text("Downgrade")
        self.click_on_button_by_text("Proceed")
        self.to_get_attribute_data_in_payment_page(data["validation_data_downgrade_free"])
        self.to_validate_next_billing_date(data["validity"])

    def test_0053_NZD_downgrade_monthly_plan_to_starter_plan(self):
        """
          Downgrade NZD monthly plan to starter plan
        """
        data = self.test_input['business_monthly_user']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.click_on_link("View invoice archive")
        self.click_on_link("View")
        details = self.get_invoice_details()
        self.to_verify_invoice(details)
        self.to_click_on_view_invoice_close_button()
        # downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_button_by_text("Downgrade")
        self.click_on_button_by_text("Proceed")
        self.to_get_attribute_data_in_payment_page(data["validation_data_downgrade_free"])
        self.to_validate_next_billing_date(data["validity"])

    def test_0054_AUD_downgrade_annual_plan_to_monthly(self):
        """
          Downgrade AUD annual plan to monthly
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        #downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_upgrade_plan("monthly")
        self.click_on_button_by_text("Update")
        data1 = self.test_input['au_business_month']
        self.to_validate_aud_upgrade_order_page(data1["payment_plan"])
        self.click_on_button("submit")
        self.to_validate_amt_details(data1["card_pay_text"])
        self.to_enter_card_details_upgrade(data1["credit_card_details_upgrade"])
        self.to_click_on_upgrade_pay_button()
        self.to_get_attribute_data_in_payment_page(data1["validation_data_downgrade_monthly"])
        self.to_validate_next_billing_date(data1["validity_downgrade_annual_monthly"])

    def test_0055_AUD_downgrade_annual_plan_to_starter(self):
        """
          Downgrade AUD annual plan to starter
        """
        data = self.test_input['au_business_annual']
        self.click_on_get_started()
        self.click_monthly_or_yearly("annually")
        self.click_on_plan("business", "annually")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        #downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_button_by_text("Downgrade")
        self.click_on_button_by_text("Proceed")
        self.to_get_attribute_data_in_payment_page(data["validation_data_downgrade_free"])
        self.to_validate_next_billing_date(data["validity"])

    def test_0056_AUD_downgrade_monthly_plan_to_starter(self):
        """
          Downgrade AUD monthly plan to starter
        """
        data = self.test_input['au_business_month']
        self.click_on_get_started()
        self.click_monthly_or_yearly("monthly")
        self.click_on_plan("business", "monthly")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.to_click_go_to_payment()
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        # downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_button_by_text("Downgrade")
        self.click_on_button_by_text("Proceed")
        self.to_get_attribute_data_in_payment_page(data["validation_data_downgrade_free"])
        self.to_validate_next_billing_date(data["validity"])

    def test_0057_AUD_downgrade_platinum_plan_to_silver_using_downgrade(self):
        """
          Downgrade AUD platinum plan to silver
        """
        data = self.test_input['aud_platinum_plan']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_au_order_page(data["payment_plan"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        #downgrade steps
        self.click_on_link("Manage my clients")
        self.to_click_toggle_option("Downgrade")
        self.click_on_link("Manage my company")
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data_downgrade_silver"])
        self.to_validate_next_billing_date(data["validity"])

    def test_0058_NZD_downgrade_platinum_plan_to_silver_using_changeplan(self):
        """
          Downgrade NZD platinum plan to starter
        """
        data = self.test_input['abb_platinum']
        self.click_on_link("For Partners")
        self.click_on_link("Learn more and sign up")
        self.to_select_abb_plan("platinum")
        self.to_fill_get_started_page(data["get_started_details"])
        self.to_validate_registration_page()
        self.to_click_on_confirmation_link()
        value = self.to_get_value_of_field_in_registration()
        self.validate_retrieved_data(value, data["industry"])
        self.to_fill_business_name(data["business_name"])
        self.to_fill_trade_n_staff(data["trade_staff"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.to_submit_registration_form()
        self.to_validate_order_page(data["payment_plan"], data["payment_gst_text"])
        self.click_on_link("Go to payment")
        self.to_validate_card_details_page(data["card_pay_text"])
        self.to_enter_card_details(data["credit_card_details"])
        self.to_click_pay_button()
        self.validate_login_bank_abb(data["tabs"])
        self.click_on_link("Manage my company")
        self.to_close_take_tour()
        self.click_settings_menu_for("Payment")
        self.to_get_attribute_data_in_payment_page(data["validation_data"])
        self.to_validate_next_billing_date(data["validity"])
        self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])
        #downgrade steps
        self.click_on_link("Change plan")
        self.verify_change_plan_page_navigation()
        self.click_on_button_by_text("Downgrade")
        self.click_on_button_by_text("Proceed")
        self.to_get_attribute_data_in_payment_page(data["validation_data_downgrade_silver"])
        self.to_validate_next_billing_date(data["validity"])

    # def test(self):
    #     data = self.test_input['business_annually_user']
    #     self.to_verify_ranqx_benchmarking_mail(data["get_started_details"]["firstname"])

    def tearDown(self):
        self.browser_close()


if __name__ == '__main__':
    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase()
    unittest.TextTestRunner(verbosity=2).run(suite)
