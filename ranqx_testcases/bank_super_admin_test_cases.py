import json
import unittest

import pkg_resources
from Automation_test.ranqx_util_files import RanqxHelper

from ranqx_tc_methods.bank_benchmark_methods import Bank_user_pages


class BankSuperAdminTestCases(unittest.TestCase, RanqxHelper, Bank_user_pages):

    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        RanqxHelper.__init__(self)
        resource_package = "test_input_details_123"  # Could be any module/package name
        resource_path = '/bank_benchmark_test_data.json'
        data = pkg_resources.resource_string(resource_package, resource_path)
        self.test_input = json.loads(data)

    def setUp(self):
        self.access_ranqx()
        print "TestCase: " + self._testMethodName
        print "Description: " + str(self._testMethodDoc)
        self.click_on_link("Sign in")
        data = self.test_input["super_admin"]
        self.enter_login_details(data["username"], data["password"])

    def test0010_super_admin_create_group(self):
        """
          Login as super admin. Create group and delete created group
        """
        data = self.test_input["super_admin"]
        self.click_on_link("Groups")
        created_grp = self.enter_group_name(data["grp_name"])
        self.enter_group_notes(data["notes"])
        print created_grp
        self.close_got_it()
        self.click_on_button("submit")
        self.verify_create_edit_delete_success(data["grp_create"])
        self.click_on_link("Groups")
        self.enter_text_in_searchfield("Group", created_grp)
        self.verify_data_in_table_after_search(created_grp)
        self.to_click_edit_delete_link("Delete")
        self.deleteform("Group")
        self.verify_create_edit_delete_success(data["grp_delete"])
        self.enter_text_in_searchfield("Group", created_grp)
        self.verify_deletion()

    def test0011_super_admin_edit_group(self):
        """
        Create, Edit, Delete of a group by super admin
        """
        data = self.test_input["super_admin"]
        self.click_on_link("Groups")
        created_grp = self.enter_group_name(data["grp_name"])
        print created_grp
        self.close_got_it()
        self.click_on_button("submit")
        self.verify_create_edit_delete_success(data["grp_create"])
        self.click_on_link("Groups")
        self.enter_text_in_searchfield("Group", created_grp)
        self.verify_data_in_table_after_search(created_grp)
        self.to_click_edit_delete_link("Edit")
        self.to_edit_group(data["edit_txt"])
        self.to_click_editform_submit()
        self.verify_create_edit_delete_success(data["grp_edit"])
        self.enter_text_in_searchfield("Group", created_grp)
        self.verify_data_in_table_after_search(created_grp)
        self.to_click_edit_delete_link("Delete")
        self.deleteform("Group")
        self.verify_create_edit_delete_success(data["grp_delete"])
        self.enter_text_in_searchfield("Group", created_grp)
        self.verify_deletion()

    def test0012_super_admin_create_staff_admin(self):
        """
           Login as super admin.
           Create staff as Admin and login as admin to verify group count same as super admin or not
         """
        data = self.test_input["super_admin"]
        data1 = data["staff_details"]
        #To get super_admin grp count
        self.click_on_link("Groups")
        sa_grp_count = self.to_get_count_of_groups()
        #create staff as admin
        self.click_on_link("Dashboard")
        self.click_on_add_new("staff")
        email = self.to_fill_form_details(data["staff_details"])
        self.select_staff_role(data["role"])
        self.close_got_it()
        self.to_activate_staff()
        self.click_on_add_staff()
        self.to_click_toggle_option("Sign out")

        #set password for created staff
        self.click_on_password_reset_link()
        self.to_enter_reset_passwords(data["reset_pwd"])
        self.wait_for(2)

        #login as admin and verify groups and customers
        self.enter_login_details(email, data["reset_pwd"])

        #To get admin grp_count
        self.click_on_link("Groups")
        admin_grp_count = self.to_get_count_of_groups()
        self.validate_grp_count(sa_grp_count, admin_grp_count)

        #Signout as Admin
        self.to_click_toggle_option("Sign out")

        #Signin as super admin
        self.enter_login_details(data["username"], data["password"])

        #To delete staff
        self.click_on_link("Staff")
        self.enter_text_in_searchfield("Staff", email)
        self.click_on_selected_staff(data["staff_details"]["firstname"])
        self.click_on_delete_icon()
        self.deleteform("Staff")
        self.verify_create_edit_delete_success(data["staff_delete"])

    def test0013_super_admin_create_staff_RM(self):
        """
        TC: super admin create staff as RM and group, assigns created staff to the group
        Validation: Login as RM and verify to which groups RM belongs to.
        Post condition: Delete created staff and group
        """
        data = self.test_input["super_admin"]

        #create staff as RM
        self.click_on_add_new("staff")
        email = self.to_fill_form_details(data["staff_rm_details"])
        self.select_staff_role(data["role_as_rm"])
        self.close_got_it()
        self.to_activate_staff()
        self.click_on_add_staff()

        #To Create new group and add staff to created group
        self.click_on_link("Groups")
        grp_name = self.enter_group_name(data["grp_name"])
        self.enter_group_notes(data["notes"])
        self.click_on_button("submit")
        self.add_staff_to_grp()
        self.select_staff_from_list_in_grps_page()
        self.click_add_staff_button_grps_page()
        self.click_save_changes_grps_page()
        self.to_click_toggle_option("Sign out")

        #set password for created staff
        self.click_on_password_reset_link()
        self.to_enter_reset_passwords(data["reset_pwd"])
        self.wait_for(2)

        #login as admin and verify groups and customers
        self.enter_login_details(email, data["reset_pwd"])
        self.click_on_add_new("customer")
        self.verify_groups_in_new_customer_page(grp_name)

        # Signout as RM
        self.to_click_toggle_option("Sign out")

        # Signin as super admin
        self.enter_login_details(data["username"], data["password"])

        # To delete staff
        self.click_on_link("Staff")
        self.enter_text_in_searchfield("Staff", email)
        self.click_on_selected_staff(data["staff_details"]["firstname"])
        self.click_on_delete_icon()
        self.deleteform("Staff")
        self.verify_create_edit_delete_success(data["staff_delete"])

        #To delete Group
        self.click_on_link("Groups")
        self.enter_text_in_searchfield("Group", grp_name)
        self.verify_data_in_table_after_search(grp_name)
        self.to_click_edit_delete_link("Delete")
        self.deleteform("Group")
        self.verify_create_edit_delete_success(data["grp_delete"])
        self.enter_text_in_searchfield("Group", grp_name)
        self.verify_deletion()

    def test0014_super_admin_create_customer(self):
        """
            To add customer to super admin and delete added customer
        """
        # To login as super admin
        data = self.test_input["super_admin"]
        #To add customer
        self.click_on_add_new("customer")
        self.to_fill_form_details(data["customer_details"])
        self.to_fill_accounting_provider(data["accountingprovider"])
        self.to_fill_business_details(data["business_details"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.click_on_button("Submit")
        self.verify_report_generation()

        #Steps to delete created customer
        customer_id = self.my_list[0]
        self.click_on_link("Dashboard")
        self.click_on_created_customer(customer_id, data["business_details"]["organisation_name"])
        self.click_on_trash()
        self.verify_create_edit_delete_success("Customer was successfully deleted.")
        self.verify_customer_deletion(customer_id)

    def test0015_super_admin_edit_customer(self):
        """
          To create, edit and delete customer
        """
         # To login as super admin
        data = self.test_input["super_admin"]
        self.click_on_add_new("customer")
        self.to_fill_form_details(data["customer_details"])
        self.to_fill_accounting_provider(data["accountingprovider"])
        self.to_fill_business_details(data["business_details"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.click_on_button("Submit")
        self.click_on_link("Dashboard")

        #To click on created customer
        customer_id = self.my_list[0]
        self.click_on_created_customer(customer_id, data["business_details"]["organisation_name"])

        # Edit Created customer
        self.click_on_edit_pencil()
        self.edit_year_of_establish(data["edit_year"])
        self.click_on_button("Submit")
        edited_value = self.get_customer_field_values("Established")
        self.validate_value(edited_value, data["edit_year"])

        # Steps to delete created customer
        self.click_on_link("Dashboard")
        self.click_on_created_customer(customer_id, data["business_details"]["organisation_name"])
        self.click_on_trash()
        self.verify_create_edit_delete_success("Customer was successfully deleted.")
        self.verify_customer_deletion(customer_id)

    def test0016_super_admin_delete_customer(self):
        """
         To delete created customer
        """
        data = self.test_input["super_admin"]
         # To add customer
        self.click_on_add_new("customer")
        self.to_fill_form_details(data["customer_details"])
        self.to_fill_accounting_provider(data["accountingprovider"])
        self.to_fill_business_details(data["business_details"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.click_on_button("Submit")

        #To delete created customer
        customer_id = self.my_list[0]
        self.click_on_link("Dashboard")
        self.click_on_created_customer(customer_id, data["business_details"]["organisation_name"])
        self.click_on_trash()
        self.verify_create_edit_delete_success("Customer was successfully deleted.")
        self.verify_customer_deletion(customer_id)

    def test0017_super_admin_generate_report_manually(self):
        """
        To generate customer report
        """
        data = self.test_input["super_admin"]
        # To add customer
        self.click_on_add_new("customer")
        self.to_fill_form_details(data["customer_details"])
        self.to_fill_accounting_provider(data["accountingprovider"])
        self.to_fill_business_details(data["business_details"])
        self.to_fill_auto_complete_data(data["autocomplete_values"])
        self.to_select_dropdown_values(data["dropdown_values"])
        self.close_got_it()
        self.click_on_button("Submit")
        self.click_on_link("Generate reports")
        self.click_on_link("Add income statement")
        self.fill_data_sheet(data["income_statement"])

        # To delete created customer
        customer_id = self.my_list[0]
        self.click_on_link("Dashboard")
        self.click_on_created_customer(customer_id, data["business_details"]["organisation_name"])
        self.click_on_trash()
        self.verify_create_edit_delete_success("Customer was successfully deleted.")
        self.verify_customer_deletion(customer_id)

    def tearDown(self):
        self.browser_close()

if __name__ == '__main__':
    unittest.main(module=__import__)