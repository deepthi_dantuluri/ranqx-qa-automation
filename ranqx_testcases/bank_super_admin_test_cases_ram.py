import pkg_resources
import json
import unittest
from ranqx_util_files.ranqx_helper import RanqxHelper
from ranqx_tc_methods.bank_benchmark_methods import Bank_user_pages


class IncomeStatement(unittest.TestCase, RanqxHelper, Bank_user_pages):
    instance = None
    data = None
    customer_id = None

    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        if IncomeStatement.instance is None:
            RanqxHelper.__init__(self)
            resource_package = 'test_input_details_123'  # Could be any module/package name
            resource_path = '/bank_benchmark_test_data.json'
            data = pkg_resources.resource_string(resource_package, resource_path)
            self.test_input = json.loads(data)
            IncomeStatement.data = self.test_input["super_admin"]
            IncomeStatement.instance = True

    @classmethod
    def setUpClass(self):
        self.a = RanqxHelper()
        self.a.access_ranqx_class()

    @classmethod
    def tearDownClass(self):
        self.a.browser_close()

    def setUp(self):
        print "TestCase: " + self._testMethodName
        print "Description:" + str(self._testMethodDoc)

    def tearDown(self):
        self.capture_screenshot()


    def create_customer(self):
        """
          Create bank customer
        """
        # To login as super admin
        self.close_got_it()
        # To add customer
        self.click_on_add_new("customer")
        self.to_fill_form_details(self.data["customer_details"])
        #print "cust id:" + IncomeStatement.customer_id
        self.to_fill_accounting_provider(self.data["accountingprovider"])
        self.to_fill_business_details(self.data["business_details"])
        self.to_fill_auto_complete_data(self.data["autocomplete_values"])
        self.to_select_dropdown_values(self.data["dropdown_values"])
        self.click_on_button("Submit")
        IncomeStatement.customer_id = self.my_list[1]
        print "cust_id 2:" + IncomeStatement.customer_id
        #self.customer_id = Bank_user_pages.my_list[0]

    # def search_select_customer(self):
    #     # To delete created customer
    #     # data = self.test_input["super_admin"]
    #     customer_email = self.my_list[0]
    #     self.click_on_link("Dashboard")
    #     self.click_on_created_customer(self.customer_id, self.data["business_details"]["organisation_name"])

    def delete_customer(self):
        # To delete created customer
        self.click_on_link("Dashboard")
        print self.customer_id
        print self.data["business_details"]["organisation_name"]
        self.click_on_created_customer(self.customer_id, self.data["business_details"]["organisation_name"])
        self.click_on_trash()
        self.verify_create_edit_delete_success("Customer was successfully deleted.")
        self.verify_customer_deletion(self.customer_id)

    def test0010_create_manual_report(self):
        self.click_on_link("Sign in")
        self.enter_login_details(self.data["username"], self.data["password"])
        self.create_customer()
        # self.verify_report_generation()
        # self.click_report_generation()
        # self.click_add_income_statement()
        # self.to_fill_data_sheet(data1["income_statement"])
        # self.click_save_income_statement()


    def test011_create_xero_integrated_report(self):
        # data = self.test_input["super_admin"]
        # self.search_select_customer()
        # self.verify_report_generation()
        # self.click_report_generation()
        # self.click_add_income_statement()
        # self.integrate_xero(data["xero"])
        # self.click_save_income_statement()

        self.delete_customer()

    # def test012_verfiy_all_elements_present_summary_mode(self):
    #     # Manual add data to income statement
    #     data = self.test_input["super_admin"]
    #     self.create_customer()
    #     self.verify_report_generation()
    #     self.click_report_generation()
    #     self.click_add_income_statement()
    #     self.to_fill_data_sheet(data["income_statement"])
    #     self.click_save_income_statement()

        # Verify income statement
        # Verify the income statement created year

        # Verify if the report title is shown and matches with the selected industry of the client

        # Verify if the income statement is in summary mode

        # Verify if graphs for Revenue, and operating profit is shown

        # Verify if the per staff revenue, operating profit and people cost graph is shown

if __name__ == '__main__':
    unittest.main(module=__import__)