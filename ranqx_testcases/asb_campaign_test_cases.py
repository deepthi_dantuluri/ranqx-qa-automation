import json
import unittest

import pkg_resources
from ranqx_util_files.ranqx_helper import RanqxHelper
from ranqx_tc_methods.asb_campaign_methods import Asb_campaign_methods


class AsbCampaign(unittest.TestCase, RanqxHelper, Asb_campaign_methods):
    instance = None
    test_input = None

    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        if AsbCampaign.instance is None:
            RanqxHelper.__init__(self)
            resource_package = "test_input_details_123"  # Could be any module/package name
            resource_path = '/asb_campaign_test_data.json'
            data = pkg_resources.resource_string(resource_package, resource_path)
            AsbCampaign.test_input = json.loads(data)
            AsbCampaign.data = AsbCampaign.test_input
            AsbCampaign.instance = True

    def setUp(self):
        self.access_ranqx()
        print "TestCase: " + self._testMethodName
        print "Description: " + str(self._testMethodDoc)

    def test0001_integrate_xero(self):
        self.click_on_link("Get your free industry report")
        self.fill_campaign_email(self.data["emailAddress"])
        self.fill_business_name(self.data["businessName"])
        self.fill_total_staff()
        self.select_industry(self.data["industry"])
        self.check_ranqx_terms()
        self.click_on_button_button("submit")
        self.click_integrate_xero(self.data["xero_signin"])

    def test0002_integrate_myob(self):
        self.click_on_link("Get your free industry report")
        self.fill_campaign_email(self.data["emailAddress"])
        self.fill_business_name(self.data["businessName"])
        self.fill_total_staff()
        self.select_industry(self.data["industry"])
        self.check_ranqx_terms()
        self.click_on_button_button("submit")
        self.click_integrate_myob(self.data["myob_signin"])

    def test0003_Manual_data(self):
        self.click_on_link("Get your free industry report")
        self.fill_campaign_email(self.data["emailAddress"])
        self.fill_business_name(self.data["businessName"])
        self.fill_total_staff()
        self.select_industry(self.data["industry"])
        self.check_ranqx_terms()
        self.click_on_button_button("submit")
        self.enter_manual_data()


    def tearDown(self):
        self.browser_close()

if __name__ == '__main__':
    unittest.main(module=__import__)