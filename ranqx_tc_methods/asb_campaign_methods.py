import time

from selenium.webdriver.common.keys import Keys
import random
from selenium.webdriver.support.ui import Select

from ranqx_util_files import outlook

class Asb_campaign_methods():

    def fill_campaign_email(self, mail_id):
        email = mail_id + str(int(time.time())) + "@test.com"
        print email
        self.driver.find_element_by_id("ranqx_campaign_customer_type_emailAddress").send_keys(email)

    def fill_business_name(self, name):
        businame = name + str(int(time.time()))
        print businame
        el = self.driver.find_element_by_id("ranqx_campaign_customer_type_businessName")
        self.scroll_to_element(el)
        el.send_keys(businame)

    def fill_total_staff(self):
        number = range(1, 100)
        staff = random.choice(number)
        print staff
        self.driver.find_element_by_id("ranqx_campaign_customer_type_fulltimeEquivalent").send_keys(staff)

    def select_industry(self, v):
        el2 = self.driver.find_element_by_id("select-industry")
        el2.send_keys(v)
        self.wait_for(1)
        el2.send_keys(Keys.DOWN, Keys.ENTER)
        #print "Value selected for " + k + " field: " + v

    def check_ranqx_terms(self):
        self.driver.find_element_by_id("ranqx_campaign_customer_type_termsCond").click()

    def click_integrate_xero(self, values):
        self.driver.find_element_by_css_selector("a[href='/xero/connect']").click()
        for k, v in values.items():
            el = self.driver.find_element_by_css_selector("input[id$=" + k + "]")
            print el
            self.scroll_to_element(el)
            el.send_keys(v)
            self.wait_for(1)
            self.wait_for(1)
        self.driver.find_element_by_id("submitButton").click()
        self.wait_for(1)
        select = Select(self.driver.find_element_by_id("organisation-select"))
        select.select_by_visible_text("Bob&#39;s Building NZ")
        self.driver.find_element_by_id("submit-button").click()
        self.wait_for(1)
        value = self.driver.find_element_by_css_selector("div.form-header.text-center").text
        print value
        self.assertEquals("All done!", value)

    def click_integrate_myob(self, values):
        self.driver.find_element_by_css_selector("a[href='/myobar/connect']").click()
        for k, v in values.items():
            el = self.driver.find_element_by_css_selector("input[id$=" + k + "]")
            print el
            self.scroll_to_element(el)
            el.send_keys(v)
            self.wait_for(1)
            self.wait_for(1)
        self.click_on_button_button("submit")
        self.wait_for(1)
        # select = Select(self.driver.find_element_by_id("organisation-select"))
        # select.select_by_visible_text("Bob&#39;s Building NZ")
        self.driver.find_element_by_id("ranqx_myob_ar_company_type_username").send_keys("administrator")
        self.click_on_button_button("submit")
        self.wait_for(1)
        value = self.driver.find_element_by_css_selector("div.form-header.text-center").text
        print value
        self.assertEquals("All done!", value)

    def enter_manual_data(self):
        self.driver.find_element_by_css_selector("a[href='#collapseTwo']").click()
        number = range(10000, 1000000)
        data = random.choice(number)
        print data
        self.driver.find_element_by_id("ranqx_campaign_customer_finance_type_totalRevenue").send_keys(data)
        self.click_on_button_button("submit")