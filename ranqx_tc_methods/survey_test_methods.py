from ranqx_util_files import outlook

class SurveyTestMethods():

    def verify_dropdown(self, txt):
        for i in range(-1, len(txt)):
            self.link_displayed_or_not(txt[i])

    def set_incentive_option(self, option):
        btn_txt = self.driver.find_element_by_css_selector(".settings-pane .btn.btn-main.grey").text
            #driver.find_element_by_xpath("//label[@class='btn btn-main grey']").text
        print btn_txt
        if btn_txt == option:
            self.driver.find_element_by_css_selector(".settings-pane .btn.btn-main.grey").click()
        else:
            print option + " already clicked"

    def incentive_pane_display(self):
        display = self.driver.find_element_by_id("incentive-pane").is_displayed()
        self.assertEquals(display, True)
        print "Incentive pane displayed"

    def incentive_pane_not_display(self):
        display = self.driver.find_element_by_id("incentive-pane").is_displayed()
        self.assertEquals(display, False)
        print "Incentive pane not displayed"

    def set_charity_incentive(self, charity):
        if charity == "Charity1":
            self.driver.find_elements_by_css_selector(".type-cp .btn.btn-main.grey")[0].click()
            print charity + " incentive is on"

        elif charity == "Charity2":
            self.driver.find_elements_by_css_selector(".type-cp .btn.btn-main.grey")[1].click()
            print charity + " incentive is on"

        elif charity == "Charity3":
            self.driver.find_elements_by_css_selector(".type-cp .btn.btn-main.grey")[2].click()
            print charity + " incentive is on"

        else:
            print "Charity not found"

    def set_charity_name(self, charity, txt):
        if charity == "Charity1":
            self.driver.find_elements_by_css_selector(".type-cp .form-control.charity-name")[0].send_keys(txt)
            print charity + " incentive is on"

        elif charity == "Charity2":
            self.driver.find_elements_by_css_selector(".type-cp .form-control.charity-name")[1].send_keys(txt)
            print charity + " incentive is on"

        elif charity == "Charity3":
            self.driver.find_elements_by_css_selector(".type-cp .form-control.charity-name")[2].send_keys(txt)
            print charity + " incentive is on"

        else:
            print "Charity not found"

    def set_amount_per_response(self, amt):
        self.driver.find_element_by_class_name("input-amount").clear()
        self.driver.find_element_by_class_name("input-amount").send_keys(amt)

    def set_max_donation_amount(self, amt):
        self.driver.find_element_by_class_name("input-amount").clear()
        self.driver.find_element_by_class_name("input-poll").send_keys(amt)

