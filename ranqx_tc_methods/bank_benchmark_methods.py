import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from ranqx_util_files import outlook


class Bank_user_pages():


#############################################Objects handled common in super admin pages#############################

    def to_click_edit_delete_link(self, txt):
        self.driver.find_element_by_link_text(txt).click()
        print "Edit link clicked"
        self.wait_for(1)

    def deleteform(self, txt):
        value = self.driver.find_element_by_id("delete" + txt + "Form").is_displayed()
        self.assertEquals(True, value)
        print "Delete" + txt + "form displayed"
        self.wait_for(2)
        self.driver.find_element_by_id("delete"+ txt + "Form"). \
            find_element_by_css_selector("input[value='Delete']").click()
        self.wait_for(1)

    def verify_create_edit_delete_success(self, txt):
        value = self.driver.find_element_by_css_selector('div.well.outlined.success').text.encode()
        result = txt in value
        self.assertEquals(True, result)
        print value

    def verify_deletion(self):
        value = self.driver.find_element_by_class_name("dataTables_empty").is_displayed()
        self.assertEquals(True, value)
        print "Deleted completely"

    def enter_text_in_searchfield(self, type, name):
        self.wait_for(1)
        self.driver.find_element_by_id("search" + type + "Input").send_keys(name)

    def verify_data_in_table_after_search(self, name):
        value = self.driver.find_element_by_link_text(name).is_displayed()
        self.assertEquals(True, value)
        print name + " exists"

################################################ objects handled in dashboard page ###################################


################################################# # objects to Handle Groups page######################################

    def enter_group_name(self, input_grp):
        element = self.driver.find_element_by_id("ranqx_bank_partner_bank_group_type_name")
        self.scroll_to_element(element)
        grp_name = input_grp + str(int(time.time()))
        element.send_keys(grp_name)
        return grp_name

    def enter_group_notes(self, input_notes):
        notes = self.driver.find_element_by_id("ranqx_bank_partner_bank_group_type_notes")
        notes.send_keys(input_notes)
        self.wait_for(2)

    ####Click on Grp creation button available in Ranqxhelper

    def to_edit_group(self, edit_txt):
        self.wait_for(1)
        self.driver.find_element_by_id("changeGroupNotes").send_keys(edit_txt)
        print "Text entered"

    def to_click_editform_submit(self):
        self.wait_for(1)
        self.driver.find_element_by_class_name("modal-content"). \
            find_element_by_css_selector("input[type='submit']").click()
        self.wait_for(1)

    def to_verify_edited_group(self, txt):
        field_text = self.driver.find_element_by_class_name("modal-content").\
            find_element_by_id("changeGroupNotes").text.encode()
        self.assertEquals(field_text, txt)
        print "Group edited successfully"
        self.driver.find_element_by_css_selector("button[type='button']").click()

    def to_get_count_of_groups(self):
        grp_count = self.driver.find_element_by_id("groupTable_info").text.encode()
        return grp_count

    def validate_grp_count(self, sa_grp_count, admin_grp_count):
        self.assertEquals(sa_grp_count, admin_grp_count)
        print "Group count is validated"


##################################################objects to Handle Staff page#1###################################
    def click_on_add_new(self, type):
        self.driver.find_element_by_link_text("Add new " + type).click()
        self.wait_for(1)
        value = self.driver.find_element_by_css_selector("input[id*='firstname']").is_displayed()
        self.assertEquals(True, value)


    def to_fill_form_details(self, details):
        """
        To fill the details in create customer form
        """
        self.my_list = []
        for k, v in details.items():
            if k == "groups":
                el = self.driver.find_element_by_class_name("select2-search__field")
                el.send_keys(v)
                self.wait_for(1)
                el.send_keys(Keys.DOWN, Keys.ENTER)
                print k + " entered as " + v

            elif k == "email":
                dynamic_email = v + str(int(time.time())) + "@test.com"
                self.driver.find_element_by_css_selector("input[id*=" + k + "]").send_keys(dynamic_email)
                print k + " entered as " + dynamic_email

            else:
                dynamic_value = v + str(int(time.time()))
                self.driver.find_element_by_css_selector("input[id*=" + k + "]").send_keys(dynamic_value)
                print k + " entered as " + dynamic_value
                print dynamic_value
                self.my_list.append(dynamic_value)
        #return dynamic_email
        return self.my_list[0]


    def select_staff_role(self, role):
        myselect = Select(self.driver.find_element_by_id("ranqx_bank_partner_bank_user_type_role"))
        myselect.select_by_visible_text(role)

    def to_activate_staff(self):
        element = self.driver.find_element_by_css_selector("input[type='checkbox']")
        self.scroll_to_element(element)
        element.click()
        self.wait_for(1)

    def click_on_add_staff(self):
        self.wait_for(1)
        self.driver.find_element_by_css_selector("input[value='Add staff']").click()
        self.wait_for(2)

    def add_staff_to_grp(self):
        element = self.driver.find_element_by_xpath("//button[contains(.,'Add staff to group')]")
        self.scroll_to_element(element)
        element.click()
        self.wait_for(1)

    def select_staff_from_list_in_grps_page(self):
        print self.my_list[2]+self.my_list[0]+'('+self.my_list[1]+')'
        myselect = Select(self.driver.find_element_by_id("ranqx_bank_partner_bank_group_list_type_users"))
        myselect.select_by_visible_text(self.my_list[2]+" "+self.my_list[0]+' ('+self.my_list[1]+')')
        self.wait_for(1)

    def click_add_staff_button_grps_page(self):
        self.driver.find_element_by_xpath(".//*[@id='addStaff']/button").click()
        self.wait_for(1)

    def click_save_changes_grps_page(self):
        self.driver.find_element_by_id("save-changes").click()
        self.wait_for(1)

    def verify_groups_in_new_customer_page(self, grpname):
        myselect = Select(self.driver.find_element_by_id("ranqx_bank_partner_bank_customer_type_groups"))
        for i in range(len(myselect.options)):
            if myselect.options[i].text == grpname:
                print "Assigned group" + grpname + " exists in Relationship manager"
            else:
                print "Fail"

    def click_on_selected_staff(self, name):
        self.driver.find_element_by_xpath(".//*[@id='staffTable']//a[contains(text()," + name + ")]").click()
        self.wait_for(1)

    def click_on_delete_icon(self):
        self.driver.find_element_by_css_selector('a[class="staff-delete"]').click()
        self.wait_for(1)


##############################Code to handle mail to reset password for staff#######################################

    def click_on_password_reset_link(self):
        self.wait()
        #self.driver.get(outlook.mark_as_read())
        self.driver.get(outlook.get_ranqx_reset_password_link())

    def to_enter_reset_passwords(self, pwd):
        self.driver.implicitly_wait(10)
        for k in ("first", "second"):
            self.driver.find_element_by_id("fos_user_resetting_form_new_" + k).send_keys(pwd)
        self.driver.find_element_by_css_selector("input[type='submit']").click()
        self.wait_for(1)
        alert_text = self. driver.find_element_by_css_selector('div.modal-body .alerts').text.encode()
        self.assertEquals("Successfully changed. Now you can log in with your new password.", alert_text)
        self.driver.find_element_by_css_selector("button.btn.btn-orange").click()


########################################Code to handle customer creation############################################

    def to_fill_accounting_provider(self, acct_provider):
        myselect = Select(self.driver.find_element_by_id("ranqx_bank_partner_bank_customer_type_accountingProvider"))
        myselect.select_by_visible_text(acct_provider)

    def to_fill_business_details(self, values):
        print "Filling business details"
        # To fill Business Name, Trading name, staff and industry
        for k, v in values.items():
            el = self.driver.find_element_by_css_selector("input[id$=" + k + "]")
            self.scroll_to_element(el)
            el.send_keys(v)
            self.wait_for(1)
            print "Value given for " + k + " field: " + v

    # To fill dropdown fields data in registration page
    def to_select_dropdown_values(self, values):
        for k, v in values.items():
            myselect = Select(self.driver.find_element_by_css_selector("[id$=" + k + "]"))
            #self.scroll_to_element(myselect)
            myselect.select_by_visible_text(v)
            self.wait_for(1)
            print "Value selected for " + k + " field: " + v

    # To fill auto complete fields in registration page
    def to_fill_auto_complete_data(self, values):
        for k, v in values.items():
            if k == "location":
                el2 = self.driver.find_element_by_id("autocomplete")
                el2.send_keys(v)
                self.wait_for(1)
                el2.send_keys(Keys.DOWN, Keys.ENTER)
                print "Value selected for " + k + " field: " + v
            else:
                el2 = self.driver.find_element_by_id("select-industry")
                el2.send_keys(v)
                self.wait_for(1)
                el2.send_keys(Keys.DOWN, Keys.ENTER)
                print "Value selected for " + k + " field: " + v

    def verify_report_generation(self):
        value = self.driver.find_element_by_link_text("Generate reports").is_displayed()
        self.assertEquals(True, value)
        print "Customer created successfully"

    def click_on_created_customer(self, cust_id, name):
        self.driver.find_element_by_id("dashboardSearch").send_keys(cust_id)
        self.wait_for(2)
        self.driver.find_element_by_link_text(name).click()

    def click_on_edit_pencil(self):
        self.driver.find_element_by_css_selector("div.controls .fa.fa-pencil").click()
        self.wait_for(1)

    def edit_year_of_establish(self, year):
        self.wait_for(1)
        myselect = Select(self.driver.find_element_by_css_selector("[id$='yearEstablished']"))
        #self.scroll_to_element(myselect)
        myselect.select_by_visible_text(year)

    def get_customer_field_values(self, key):
        el = self.driver.find_elements_by_xpath("//div[@class='row detail-item']")
        el1 = self.driver.find_elements_by_xpath("//div[@class='col-sm-6 detail-label']")
        el2 = self.driver.find_elements_by_xpath("//div[@class='col-sm-6 detail-data']")
        for i in range(0, len(el)-1):
            if el1[i].text == key:
                print el2[i].text
                return el2[i].text

    def validate_value(self, value1, value2):
        self.assertEquals(value1, value2)
        print "Value edited successfully"

    def click_on_trash(self):
        self.driver.find_element_by_css_selector("div.controls .fa.fa-trash-o").click()
        self.wait_for(1)
        self.driver.find_element_by_xpath(".//*[@id='deleteStaffForm']/div[3]/input").click()
        self.wait_for(1)

    def verify_customer_deletion(self, cust_id):
        self.driver.find_element_by_id("dashboardSearch").send_keys(cust_id)
        self.wait_for(1)
        value = self.driver.find_element_by_class_name("dataTables_empty").is_displayed()
        self.assertEquals(True, value)
        print cust_id + " deleted"

#########################################To fill data in income sheet###########################################

    def fill_data_sheet(self, values):
        for k, v in values.items():
            self.fill_text_field(k, v)
            self.wait_for(1)
