import re
import time
from datetime import date

from dateutil.relativedelta import relativedelta
from selenium.common.exceptions import ElementNotVisibleException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from ranqx_util_files import outlook


class SignupUserMethods():

    # To click Create an account link
    def click_on_get_started(self):
        self.wait_for(2)    # to be removed
        self.driver.find_element_by_xpath("html/body/footer/div/div[1]/div[1]/div[2]/a").click()
        print "clicked on sign up button"

    # To click on required sign up plan
    def click_on_plan(self, plan, term):
        self.wait_for(2)     # to be removed
        element = self.driver.find_element_by_class_name(plan).find_element_by_css_selector('a.sign-up.' + term)
        self.scroll_to_element(element)
        element.click()
        print "clicked on sign-up for " + plan + " plan"

    def click_on_starter_plan(self, plan):
        self.wait_for(2)
        element = self.driver.find_element_by_class_name(plan).find_element_by_css_selector('a.sign-up')
        self.scroll_to_element(element)
        element.click()
        print "clicked on sign-up for " + plan + " plan"

    # To fill get started page details
    def to_fill_get_started_page(self, values):
        print "Filling get started details"
        for k, v in values.items():
            if k != "email":
                self.driver.find_element_by_id("fos_user_registration_form_" + k).send_keys(v)
            else:
                email = self.driver.find_element_by_id("fos_user_registration_form_" + k)
                email.send_keys(v + str(int(time.time())) + "@test.com")
        self.driver.find_element_by_class_name("tickbox").click()
        self.driver.find_element_by_class_name("submit-next").click()

    # To Validate registration page
    def to_validate_registration_page(self):
        if self.driver.find_element_by_xpath("html/body/div[1]/div/div/h1").text == "Registration":
            email_id = self.driver.find_element_by_xpath("html/body/div[1]/div/div/p[2]/b").text
            print "Confirmation mail sent to id: " + email_id

    def to_make_mails_as_read(self):
        self.driver.get(outlook.mark_as_read())

    # To fill business details as part of registration process
    def to_fill_trade_n_staff(self, values):
        print "Filling business details"
        # To fill Business Name, Trading name, staff and industry
        for k, v in values.items():
            element = self.driver.find_element_by_id("fos_user_registration_form")
            element.find_element_by_id("webfit_ranqx_corebundle_organisation_" + k).send_keys(v)
            self.wait_for(1)
            print "Value given for " + k + " field: " + v

    def to_fill_business_name(self, values):
        for k, v in values.items():
            changed_value = str(int(time.time()))+v
            self.fill_text_field(k, changed_value)
        print "Value given for " + k + " field: " + v

    # To fill dropdown fields data in registration page
    def to_select_dropdown_values(self, values):
        for k, v in values.items():
            myselect = Select(self.driver.find_element_by_id("webfit_ranqx_corebundle_organisation_" + k))
            myselect.select_by_visible_text(v)
            self.wait_for(1)
            print "Value selected for " + k + " field: " + v

    # To fill auto complete fields in registration page
    def to_fill_auto_complete_data(self, values):
        for k, v in values.items():
            if k == "location":
                el2 = self.driver.find_element_by_id("autocomplete")
                el2.send_keys(v)
                self.wait_for(1)
                el2.send_keys(Keys.DOWN, Keys.ENTER)
                print "Value selected for " + k + " field: " + v
            else:
                el2 = self.driver.find_element_by_id("select-industry")
                el2.send_keys(v)
                self.wait_for(1)
                el2.send_keys(Keys.DOWN, Keys.ENTER)
                print "Value selected for " + k + " field: " + v

    # To click on Submit button after filling registration details
    def to_submit_registration_form(self):
        self.wait_for(5)
        element = self.driver.find_element_by_id("webfit_ranqx_corebundle_organisation_submit")
        self.scroll_to_element(element)
        element.click()

    # To validate free user after signup
    def to_validate_free_user_signup(self):
        self.to_close_take_tour()
        self.validate_login()
        try:
            self.driver.find_element_by_link_text("Upgrade now for more features")
            self.assertTrue(True, "upgrade link available")
            print "Plan upgrade link is available"
        except ElementNotVisibleException:
            self.assertTrue(False, "upgrade link is not available")

    # To handle take tour window
    def to_close_take_tour(self):
            try:
                self.wait_for(2)
                self.driver.\
                    find_element_by_xpath("//*[@id='taketour']//span[@class='glyphicon glyphicon-remove pull-right']").\
                    click()
                print "Take tour window closed"
                self.wait_for(2)
            except ElementNotVisibleException:
                self.assertTrue(False, "Take tour window is not displayed")

    def click_monthly_or_yearly(self, plan_type):
        self.wait_for(2)
        self.driver.find_element_by_class_name("switch").find_element_by_id(plan_type).click()

    def to_validate_order_page(self, input_plan, input_gst_text):
        try:
            # To validate heading of payment page
            self.wait_for(1)
            self.driver.find_element_by_xpath('html/body/section/div/div/div/div[1]/div/h2')
            self.assertTrue(True, "Review your order heading not present")
            print "Review your order text is present"
            # To validate plan_type
            plan_type = self.driver.find_element_by_class_name("plan-type").text
            self.assertIn(input_plan, plan_type)
            # To validate promo-code
            self.driver.find_element_by_id("promo-code")
            self.assertTrue(True, "Promo code field is available")
            print "Promo code is available"
            # To validate GST
            gst_text = self.driver.find_element_by_xpath("html/body/section/div/div/div/div[4]/div/div/div/span").text
            self.assertEquals(input_gst_text, gst_text)
            print "Showing following text on order page: " + gst_text
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_validate_order_page_after_promo_code(self, input_discount, discount_gst):
        try:
            #To validate discount
            dis_amt = self.driver.find_element_by_class_name("discount").text
            self.assertEquals(dis_amt, input_discount)
            print "Showing following text on order page: " + dis_amt
            # To validate GST
            gst_text = self.driver.find_element_by_xpath("html/body/section/div/div/div/div[5]/div/div/div/span").text
            self.assertEquals(gst_text, discount_gst)
            print "Showing following text on order page: " + gst_text
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_click_go_to_payment(self):
        self.driver.find_element_by_class_name("reg-submit").click()

    def to_validate_card_details_page(self, pay_btn_text):
        try:
            # To validate heading of credit card details page
            head_text = self.driver.find_element_by_xpath('html/body/div/div/h2').text
            #head_text = self.driver.find_element_by_xpath("//button[contains(text(),'Pay')]").text
            self.assertEquals("Please complete your card payment details", head_text)
            print "Credit card details heading displayed correctly"
            #
            self.driver.find_element_by_id("payment_name")
            self.assertTrue(True, "Payment Name field exists")
            print "Payment Name field exists"
            # To validate text on Pay button
            pay_details = self.driver.find_element_by_class_name("submit").text
            self.assertEquals(pay_details, pay_btn_text)
            print "Pay button displayed: " + pay_details
        except ElementNotVisibleException:
            self.assertTrue(False, "credit card page failed validation")
            print "credit card page failed validation"

    def to_enter_card_details(self, values):
        for k, v in values.items():
            if (k == "payment_yy") or (k == "payment_mm"):
                self.driver.find_element_by_xpath("//select[@id='" + k + "']/option[text()='" + str(v) + "']").click()
            else:
                self.driver.find_element_by_id(k).send_keys(v)

    def to_click_pay_button(self):
        self.wait_for(1)
        self.driver.find_element_by_css_selector('button.btn.btn-orange.pull-right').click()


    def to_get_attribute_data_in_payment_page(self, input_value):
        self.wait_for(2)
        for k, v in input_value.items():
            table_row = self.driver.find_elements_by_tag_name("tr")
            for i in range(1, len(table_row)):
                if table_row[i].find_elements_by_tag_name("td")[0].text == k:
                    print table_row[i].find_elements_by_tag_name("td")[0].text
                    col_text = table_row[i].find_elements_by_tag_name("td")[1].text
                    self.validate_retrieved_data(col_text, v)
                    break

    def to_validate_next_billing_date(self, validity):
        val = date.today() + relativedelta(months=+int(validity))
        day = str(val.day)
        year = str(val.year)
        month = val.strftime('%b')
        #print validity_date
        table_row = self.driver.find_elements_by_tag_name("tr")
        for i in range(0, len(table_row)):
            if table_row[i].find_elements_by_tag_name("td")[0].text == "Next billing date":
                col_text = table_row[i].find_elements_by_tag_name("td")[1].text
                m = re.search(day + '(th|st|rd|nd)\s' + month + '\s' + year, col_text)
                self.validate_retrieved_data(col_text, m.group(0))
                break

    def to_validate_au_order_page(self, input_plan):
        try:
            # To validate heading of payment page
            self.wait_for(1)
            self.driver.find_element_by_xpath('html/body/section/div/div/div/div[1]/div/h2')
            self.assertTrue(True, "Review your order heading not present")
            print "Review your order text is present"
            # To validate plan_type
            plan_type = self.driver.find_element_by_class_name("plan-type").text
            self.assertIn(input_plan, plan_type)
            print plan_type
            # To validate promo-code
            self.driver.find_element_by_id("promo-code")
            self.assertTrue(True, "Promo code field is available")
            print "Promo code is available"
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_validate_au_order_page_after_pc(self, input_plan, input_dis_amt):
        try:
            # To validate plan_type
            plan_type = self.driver.find_element_by_class_name("plan-type").text
            self.assertIn(input_plan, plan_type)
            print plan_type
            dis_amt = self.driver.find_element_by_class_name("discount").text
            self.assertIn(dis_amt, input_dis_amt)
            print dis_amt
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_enter_promo_code(self, code):
        self.driver.find_element_by_id("promo-code").send_keys(code)
        self.wait_for(1)
        self.driver.find_element_by_class_name("input-group-btn").click()
        self.wait_for(1)

    def to_click_continue(self):
        self.driver.find_element_by_link_text("Continue").click()
        self.wait_for(2)

    def to_select_abb_plan(self, plan):
        self.driver.find_element_by_css_selector("div[data-addon=" + plan + "]").click()

    def to_get_value_of_field_in_registration(self):
        txt = self.driver.find_element_by_id("select-industry").get_attribute('value').encode()
        return txt

    def validate_retrieved_data(self, field_txt, input_txt):
        self.assertEquals(field_txt, input_txt)
        print field_txt + " Matches " + input_txt

############################### Functions for upgrade######################################################

    def click_on_upgrade_plan(self, txt):
        if txt == "monthly":
            self.driver.find_elements_by_tag_name("label")[0].click()
        else:
            self.driver.find_elements_by_tag_name("label")[1].click()

    def click_on_button_by_text(self, btn_txt):
        self.wait_for(1)
        ele = self.driver.find_element_by_xpath("//*[contains(text(),'" + btn_txt + "')]")
        ele.click()
        print "Clicked on " + btn_txt

    def to_validate_amt_details(self, pay_btn_text):
        pay_details = self.driver.find_element_by_class_name("submit").text.encode()
        self.assertEquals(pay_details, pay_btn_text)
        print "Pay button displayed: " + pay_details

    def to_click_on_upgrade_pay_button(self):
        self.wait_for(1)
        self.driver.find_element_by_css_selector("button[type=submit]").click()

    def to_enter_card_details_upgrade(self, values):
        for k, v in values.items():
            if (k == "creditcard_yy") or (k == "creditcard_mm"):
                self.driver.find_element_by_xpath("//select[contains(@id,'" + k + "')]/option[text()='" + str(v) + "']")\
                    .click()
            else:
                self.driver.find_element_by_xpath("//input[contains(@id,'" + k + "')]").send_keys(v)

    def to_validate_upgrade_order_page(self, input_plan, input_gst_text):
        try:
            # To validate plan_type
            plan_type = self.driver.find_element_by_xpath("//*[@class='form']/div/div[2]/div/div").text
            self.assertIn(input_plan, plan_type)
            # To validate promo-code field
            promo_code_field = self.driver.find_element_by_class_name("form-control").is_displayed()
            self.assertEquals(True, promo_code_field)
            print "Promo code is available"
            # To validate GST
            gst_text = self.driver.find_element_by_xpath("//*[@class='payment-total']/span").text
            self.assertEquals(input_gst_text, gst_text)
            print "Showing following text on order page: " + gst_text
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_validate_aud_upgrade_order_page(self, input_plan):
        try:
            # To validate plan_type
            plan_type = self.driver.find_element_by_xpath("//*[@class='form']/div/div[2]/div/div").text
            self.assertIn(input_plan, plan_type)
            # To validate promo-code field
            promo_code_field = self.driver.find_element_by_class_name("form-control").is_displayed()
            self.assertEquals(True, promo_code_field)
            print "Promo code is available"
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_click_on_view_invoice_close_button(self):
        self.driver.find_element_by_xpath(".//*[@id='invoice-window']/div/div/div[3]/button").click()

    def to_enter_promo_code_upgrade_page(self, code):
        self.driver.find_element_by_class_name("form-control").send_keys(code)
        self.wait_for(1)
        self.driver.find_element_by_class_name("input-group-btn").click()
        self.wait_for(1)

    def to_validate_upgrade_order_page_after_pc(self, input_plan, input_gst_text, input_dis):
        try:
            # To validate plan_type
            plan_type = self.driver.find_element_by_xpath("//*[@class='form']/div/div[2]/div/div").text
            self.assertIn(input_plan, plan_type)
            # To validate promo-code field
            promo_code_field = self.driver.find_element_by_xpath("//*[@class='form']/div/div[3]/div/div").text
            self.assertEquals(promo_code_field, input_dis)
            print "Promo code discount" + promo_code_field
            # To validate GST
            gst_text = self.driver.find_element_by_xpath("//*[@class='payment-total']/span").text
            self.assertEquals(input_gst_text, gst_text)
            print "Showing following text on order page: " + gst_text
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_validate_au_upgrade_order_page_after_pc(self, input_plan, input_dis):
        try:
            # To validate plan_type
            plan_type = self.driver.find_element_by_xpath("//*[@class='form']/div/div[2]/div/div").text
            self.assertIn(input_plan, plan_type)
            # To validate promo-code field
            promo_code_field = self.driver.find_element_by_xpath("//*[@class='form']/div/div[3]/div/div").text
            self.assertEquals(promo_code_field, input_dis)
            print "Promo code discount" + promo_code_field
        except ElementNotVisibleException:
            self.assertTrue(False, "Order page failed validation")

    def to_validate_text_in_payment_page(self, text):
        payment_pg_text = self.driver.find_element_by_class_name("under-heading").text
        self.assertEquals(payment_pg_text, text)
        print "Text under heading in payment page displayed correctly"