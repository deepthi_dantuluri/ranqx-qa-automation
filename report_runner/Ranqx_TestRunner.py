import os
import unittest

from ranqx_testcases.signup_user_test_cases import SignupUsersTestCases
from ranqx_testcases.survey_test_cases import SurveyTestCases
from ranqx_util_files import HTMLTestRunner

# get the directory path to output report file
dir = os.getcwd()

# get all tests from test cases
#existing_users_login_scripts = unittest.TestLoader().loadTestsFromTestCase(RanqxLoginExistingUsersTestCases)
SurveyTestCases = unittest.TestLoader().loadTestsFromTestCase(SurveyTestCases)
#bank_super_admin_cases = unittest.TestLoader().loadTestsFromTestCase(BankSuperAdminTestCases)
#unittest.TextTestRunner(verbosity=2).run(existing_users_login_scripts)
# create a test suite combining all tests
Regression_tests = unittest.TestSuite(SurveyTestCases)
#unittest.TestSuite([existing_users_login_scripts, signup_test_cases])

# open the report file
outfile = open(dir + "/RanqxTestReport.html", "w")

# configure HTMLTestRunner options
runner = HTMLTestRunner.HTMLTestRunner(
                 stream=outfile,
                 title='Ranqx Test Report',
                 description='Test execution to verify existing user login'
                 )

# run the suite using HTMLTestRunner
runner.run(Regression_tests)