import json
import sys
from selenium import webdriver
from time import sleep
import os
import commands
from datetime import datetime
import pkg_resources
import outlook


class RanqxHelper():

    driver = None
    env = None
    session = None
    # To Get data from json file
    def __init__(self):
        print "In ranqx init"
        resource_package = 'test_input_details_123' # Could be any module/package name
        resource_path = '/test_env_details.json'  # Do not use os.path.join(), see below
        data = pkg_resources.resource_string(resource_package, resource_path)
        data_dictionary = json.loads(data)
        self.test_env_data = data_dictionary
        RanqxHelper.driver = data_dictionary['browser']
        RanqxHelper.env = data_dictionary['env']

    #To access ranqx test environment on specified browser
    def access_ranqx(self):
        print "**************************************************************"
        print "Test running on " + self.driver + ", environment: " + self.env
        if self.driver == "firefox":
            geoDisabled = webdriver.FirefoxProfile()
            geoDisabled.set_preference("geo.enabled", False)
            self.driver = webdriver.Firefox(geoDisabled)
            self.driver.implicitly_wait(10)

        elif self.driver == "chrome":
            chromedriver = "../chromedriver"
            self.driver = webdriver.Chrome(chromedriver)
        else:
            self.driver = webdriver.Ie()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.enter_url(RanqxHelper.env)

    def access_ranqx_class(self):
        print "**************************************************************"
        print "Test running on " + self.driver + ", environment: " + self.env
        if RanqxHelper.driver == "firefox":
            geoDisabled = webdriver.FirefoxProfile()
            geoDisabled.set_preference("geo.enabled", False)
            RanqxHelper.driver = webdriver.Firefox(geoDisabled)
            RanqxHelper.driver.implicitly_wait(10)

        elif RanqxHelper.driver == "chrome":
            chromedriver = "../chromedriver"
            RanqxHelper.driver = webdriver.Chrome(chromedriver)
        else:
            RanqxHelper.driver = webdriver.Ie()
        RanqxHelper.driver.implicitly_wait(30)
        RanqxHelper.driver.maximize_window()
        self.enter_url(RanqxHelper.env)

    # To enter ranqx url
    def enter_url(self, ranqx_url):
        self.driver.get(ranqx_url)

    # Method to click on link objects:
    def click_on_link(self, txt):
        self.wait_for(1)
        self.driver.find_element_by_link_text(txt).click()
        print "clicked on: " + txt
        self.wait_for(1)

    def verify_change_plan_page_navigation(self):
        ele = self.driver.find_element_by_link_text("Return to Settings").is_displayed()
        self.assertEquals(True, ele)
        print "Navigated to Change plan"

    # To click Submit button using Css selector
    def click_on_button(self, txt):
        btn_element = self.driver.find_element_by_css_selector("input[type=" + txt + "]")
        self.scroll_to_element(btn_element)
        self.wait_for(1)
        btn_element.click()
        self.wait_for(2)
        print "Button clicked"

    def capture_screenshot(self):
        test_method_name = self._testMethodName
        if sys.exc_info()[0]:  # Returns the info of exception being handled
            now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f')
            os.chdir("..")
            status, path = commands.getstatusoutput("pwd")
            #print path
            status, output = commands.getstatusoutput("mkdir ./screenshots")
            match = 'File exists' in output
            if match == "False":
                os.makedirs('./screenshots')
                self.wait_for(1)
            else:
                print "inside else"
                os.chdir('./screenshots')
                print os.system('pwd')
            self.driver.get_screenshot_as_file('../screenshots/' + test_method_name + '_%s.png' % now)
            fail_screenshot_url = 'https:' + path + '/screenshots/' + test_method_name + '_%s.png' % now
            print fail_screenshot_url
        else:
            print test_method_name + ": Pass"

    # To close the browser
    def browser_close(self):
        # if sys.exc_info()[0]:  # Returns the info of exception being handled
        #     test_method_name = self._testMethodName
        #     now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f')
        #     os.chdir("..")
        #     status, path = commands.getstatusoutput("pwd")
        #     #print path
        #     status, output = commands.getstatusoutput("mkdir ./screenshots")
        #     match = 'File exists' in output
        #     if match == "False":
        #         os.makedirs('./screenshots')
        #         self.wait_for(1)
        #     else:
        #         print "inside else"
        #         os.chdir('./screenshots')
        #         print os.system('pwd')
        #     self.driver.get_screenshot_as_file('../screenshots/' + test_method_name + '_%s.png' % now)
        #     fail_screenshot_url = 'https:' + path + '/screenshots/' + test_method_name + '_%s.png' % now
        #     print fail_screenshot_url
        self.driver.quit()

    # To enter login credentials specified in test case data files
    def enter_login_details(self, login_name, login_password):
        self.driver.find_element_by_id("username").send_keys(login_name)
        self.driver.find_element_by_id("password").send_keys(login_password)
        self.driver.find_element_by_id("_submit").click()
        self.driver.implicitly_wait(10)
        print self.driver.title
        print "Successfully logged in as: " + login_name

    # To click on the toggle for some operations like logout
    def to_click_toggle_option(self, operation):
        self.wait_for(2)
        self.driver.find_element_by_css_selector('a.dropdown-toggle').click()
        self.driver.find_element_by_link_text(operation).click()
        print "Clicked on: " + operation
        self.driver.implicitly_wait(10)

    # To validate login by verifying Dasboard title
    def validate_login(self):
        self.assertIn("Ranqx - Dashboard", self.driver.title)
        print "User logged in successfully"

    # To validate logout by verifying title
    def validate_logout(self):
        self.assertIn("Ranqx - Sign in", self.driver.title)
        print "User logged out successfully"

    # After navigating to settings to click on required settings option
    def click_settings_menu_for(self, settings_menu_item):
        self.to_click_toggle_option("Settings")
        self.wait_for(5)
        self.driver.find_element_by_link_text(settings_menu_item).click()

    # Verifying page header for abb and bank sign-ups
    def verify_page_header(self, page_title):
        header_text = self.driver.find_element_by_class_name('navbar-header').text
        self.assertEqual(page_title, header_text)
        print "Header text matched with business name:" + page_title

    def wait(self):
        self.driver.implicitly_wait(50)

    def wait_for(self, time):
        sleep(time)

    def scroll_to_element(self, element):
        self.driver.execute_script("return arguments[0].scrollIntoView();", element)
        self.driver.execute_script("window.scrollBy(0, -150);")

    def close_got_it(self):
        element = self.driver.find_element_by_css_selector("div.herbyCookieConsent.herbyIn .herbyBtn")
        self.wait_for(1)
        value = element.is_displayed()
        if value == True:
            element.click()
        else:
            pass

    def fill_text_field(self, key, value):
        self.driver.find_element_by_css_selector("input[id$=" + key + "]").send_keys(value)

    def validate_login_bank_abb(self, values):
        print values[0] + " owner home page has below tabs:"
        for i in range(1, len(values)):
            menu = self.driver.find_element_by_link_text(values[i]).is_displayed()
            self.assertEquals(menu, True)
            print values[i]

##############################Outlook related methods######################################################

    # To click confirmation link from mail
    def to_click_on_confirmation_link(self):
        self.wait()
        self.driver.get(outlook.get_ranqx_signup_link())
        self.assertIn("Ranqx - Registration", self.driver.title)

    def to_verify_invoice(self, name):
        outlook.verify_payment_invoice(name)

    def to_verify_ranqx_benchmarking_mail(self, name):
        outlook.verify_ranqx_benchmarking(name)

    def get_invoice_details(self):
        print "Verifying Invoice Details"
        words_list = []
        for i in range(1, 3):
            data = self.driver.find_element_by_xpath(".//*[@id='invoice-head']/p[" + str(i) + "]").text
            words = data.split("\n")
            for j in range(0, len(words)):
                words_list.append(words[j])

        # To get Invoice body
        rows = len(self.driver.find_elements_by_xpath(".//*[@id='invoice-body']/tr"))
        for i in range(1, rows+1):
            cols = len(self.driver.find_elements_by_xpath(".//*[@id='invoice-body']/tr[" + str(i) + "]/td"))
            for j in range(1, cols+1):
                ele = self.driver.find_element_by_xpath(".//*[@id='invoice-body']/tr[" + str(i) + "]\
                /td[" + str(j) + "]").text
                ele_1 = ele.split("-")
                for y in range(0, len(ele_1)):
                    words_list.append(ele_1[y])

        # To get business name
        business_name = self.driver.find_element_by_id("organisation-name").text
        words_list.append(business_name)
        return words_list

    def link_displayed_or_not(self, text):
        value = self.driver.find_element_by_link_text(text).is_displayed()
        self.assertEquals(value, True)
        print text + " exists"