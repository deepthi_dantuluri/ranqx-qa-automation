# -*- coding: utf-8 -*-
# -*- coding: <encoding name> -*-
import imaplib
import email
import smtplib
import datetime
import email.mime.multipart
import re
import time

mail = imaplib.IMAP4_SSL('outlook.office365.com')
mail.login('test@ranqx.com', 'NIK9+La*')
mail.list()
# Out: list of "folders" aka labels in Outlook.
mail.select("Inbox") # connect to inbox.

date = (datetime.date.today() - datetime.timedelta(1)).strftime("%d-%b-%Y")
# result, data = mail.uid('search', None, '(UNSEEN)','(SENTSINCE {date} HEADER Subject "ranqx email verification")'.format(date=date))
# Thanks for requesting a demo on ranqx
# Ranqx Payment Invoice
# Please join us on ranqx
# Set up a new password
# Invite Accepted
# Have your say
# Please confirm your email to use Maestrano
# latest_email_uid = data[0].split()[-1]
# result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
# raw_email = data[0][1]
# email_message = email.message_from_string(raw_email)

# print email_message
# print email_message['To']
# print email.utils.parseaddr(email_message['From']) # for parsing "Ram K" <ram.karuppiah@ranqx.com>
# print email_message.items() # print all headers

# note that if you want to get text content (body) and the email contains
# multiple payloads (plaintext/ html), you must parse each message separately.
# use something like the following: (taken from a stackoverflow post)

# below method is needed for converting the data ...Something required for Survey emails and Maestrano??
def get_first_text_block(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.walk():
            if part.get_content_maintype() == 'text/html':
                return part.get_payload()
    elif maintype == 'text/html':
        return email_message_instance.get_payload()
        
# Below method will mark all mails as read, precaution to make all mail as read, otherwise test methods fail
def mark_as_read():
    # type: () -> object
    result, data = mail.search(None,'UNSEEN')
    if data != ['']:
        mail.store(data[0].replace(' ',','),'+FLAGS','\SEEN')
    else:
        pass

# Below method works to get account activation signup from Ranqx Staging with Webfit
def get_ranqx_staging_signup_link():
    wait_time=0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None,'(UNSEEN)','(SENTSINCE {date} HEADER Subject "ranqx email verification")'.format(date=date))
        wait_time= wait_time+3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranqx.webfit.co.nz/register/confirm/.*)', str(email_message))
    if m is None: #Alternative method by Kev
        print 'No Link Found' 
    else:
        return m.group(0)
        
    mail.close()
    mail.logout()

# Below method works to get survey link from Ranqx Staging with Webfit
def get_ranqx_staging_survey_link():
    wait_time=0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)','(SENTSINCE {date} HEADER Subject "Have your say – and help a charity")'.format(date=date))
        wait_time= wait_time+3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranqx.webfit.co.nz/survey/.*)', str(email_message))
    if m is None: #Alternative method by Kev
        print 'No Link Found' 
    else:
        return m.group(0)
        
    mail.close()
    mail.logout()
    
def get_ranqx_staging_demo_link():
    wait_time=0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None,'(UNSEEN)','(SENTSINCE {date} HEADER Subject "Thanks for requesting a demo on ranqx")'.format(date=date))
        wait_time= wait_time+3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranqx.webfit.co.nz/demo/verify/[^\s]+)', str(email_message))
    if m is None: #Alternative method by Kev
        print 'No Link Found' 
    else:
        return m.group(0)
        
    mail.close()
    mail.logout()

# Below method works to get signup link from feature branch
def get_ranqx_feature_signup_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "ranqx email verification")'.format(date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(rdp.*.ranqx.io/register/confirm/[^\s]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()


# Below method works to get survey link from feature branch
def get_ranqx_feature_survey_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Have your say – and help a charity")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(rdp.*.ranqx.io//survey/.*)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

# Below method works to get survey link from feature branch
def get_ranqx_feature_demo_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Thanks for requesting a demo on ranqx")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(rdp.*.ranqx.io/demo/verify/[^\s]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

# Below method works to get reset password link from feature branch
def get_ranqx_reset_password_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Set up a new password")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('((http[s]?:)?\/\/)?((rdp|develop).*.ranqx.io/resetting/reset/[^\s][^\>]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(3)

    mail.close()
    mail.logout()

# Below method works to get signup link from uat
def get_ranqx_signup_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "ranqx email verification")'.format(date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?((rdp|develop|uat).*.ranqx.io/register/confirm/.*)', str(email_message))
    #m = re.search(env + "/register/confirm/.*", str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

# Below method works to get survey link from uat
def get_ranqx_uat_survey_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Have your say – and help a charity")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(uat.ranq.io//survey/.*)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

# Below method works to get demo link from uat
def get_ranqx_uat_demo_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Thanks for requesting a demo on ranqx")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(uat.ranqx.io/demo/verify/[^\s]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

# Below method works to get reset password link from uat
def get_ranqx_uat_reset_password_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Set up a new password")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(uat.ranqx.io/resetting/reset/[^\s][^\>]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()


# Below method works to get signup link from live
def get_ranqx_live_signup_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "ranqx email verification")'.format(date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranqx.com/register/confirm/.*)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()


# Below method works to get survey link from live
def get_ranqx_live_survey_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Have your say – and help a charity")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranq.com//survey/.*)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()


# Below method works to get demo link from live
def get_ranqx_live_demo_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Thanks for requesting a demo on ranqx")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranqx.com/demo/verify/[^\s]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

# Below method works to get reset password link from live
def get_ranqx_live_reset_password_link():
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Set up a new password")'.format(
                                    date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    # email_body = get_first_text_block(email_message_instance)#.decode('base64') ...decode is needed if mail is in base 64 (Ex.forwarded mails)
    m = re.search('(http[s]?:\/\/)?(ranqx.com/resetting/reset/[^\s]+)', str(email_message))
    if m is None:  # Alternative method by Kev
        print 'No Link Found'
    else:
        return m.group(0)

    mail.close()
    mail.logout()

def verify_payment_invoice(text):
    wait_time = 0
    value = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Ranqx Payment Invoice")'.format(date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    print "Text in invoice mail: "
    for i in range(0, len(text)):
        try:
            m = re.search(text[i], str(email_message))
            print m.group(0)
        except AttributeError:
            WARNING = "\033[1;31m"
            ENDC = '\033[0m'
            print WARNING + "Value not found in mail: " + text[i] + ENDC
            value = 1
    print "value: " + str(value)
    assert (value != 1), "Failed at email invoice data verification step"

def verify_ranqx_benchmarking(text):
    wait_time = 0
    data = ['']
    while (wait_time < 60) and data == ['']:
        result, data = mail.uid('search', None, '(UNSEEN)',
                                '(SENTSINCE {date} HEADER Subject "Welcome to Ranqx benchmarking")'.format(date=date))
        wait_time = wait_time + 3
        time.sleep(3)
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_string(raw_email)
    try:
        m = re.search(text, str(email_message))
        print m.group(0)
    except AttributeError:
        WARNING = "\033[1;31m"
        ENDC = '\033[0m'
        print WARNING + "Value not found in mail: " + text + ENDC
